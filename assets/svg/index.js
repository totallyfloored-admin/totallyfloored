export { default as EmailIcon } from './email-icon.svg';
export { default as GoogleIcon } from './google-icon.svg';
export { default as AppleIcon } from './apple-icon.svg';