import React from "react"
import { View, ViewStyle, TextStyle, Text } from "react-native"
import { Screen } from "../../components"

export const LoadingScreen = () => {

  return (
    <Screen style={ROOT}>
        <Text style={TITLE}>🏄🏼‍♂️ Loading
        </Text>
    </Screen>
  )
}

const ROOT: ViewStyle = {
  backgroundColor: `white`,
  flex: 1,
}

const TITLE: TextStyle = {
    flex: 1,
    fontSize: 32,
    fontWeight: `600`,
    marginHorizontal: 32,
    marginTop: 40
}