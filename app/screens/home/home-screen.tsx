import React, { useState, useEffect, useRef } from "react"
import { View, ViewStyle, TextStyle, Text } from "react-native"
import { Screen, Button } from "../../components"
import { useAuthContext } from '../../context/auth-context'
import auth from '@react-native-firebase/auth';

export const HomeScreen = () => {
  const authContext = useAuthContext();
  const authUser = authContext.authUser;

  const logout = () => {
    auth()
    .signOut()
    .then(() => console.log('User signed out!'));
  }

  return (
    <Screen style={ROOT}>
        <Text style={TITLE}>👋 Welcome,{"\n"}
            <Text style={TITLE_USER}>{`${authUser?.email}`}</Text>
        </Text>
        <Button 
            text="log out"
            style={{ marginBottom: 60, marginHorizontal: 48 }}
            onPress={logout}
        />
    </Screen>
  )
}

const ROOT: ViewStyle = {
  backgroundColor: `white`,
  flex: 1,
}

const TITLE: TextStyle = {
    flex: 1,
    fontSize: 32,
    fontWeight: `600`,
    marginHorizontal: 32,
    marginTop: 40
}

const TITLE_USER: TextStyle = {
    fontWeight: `400`,
    color: `grey`
}