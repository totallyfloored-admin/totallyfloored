import React, { useState, useEffect, useRef } from "react"
import { View, ViewStyle, Animated, ImageBackground } from "react-native"
import { Screen, LogInHome, SignInEmail, CreateAccountHome, CreateAccountEmail } from "../../components"
import { AddAccountDetails } from "../../components/auth/add-account-details"
import { SecurityQuestions } from "../../components/auth/security-questions"
import { useNavigation } from "@react-navigation/native"
import { color } from "../../theme"
import { useAuthContext } from '../context/auth-context'

export const LoginScreen = () => {

  const navigation = useNavigation();
  const authContext = useAuthContext();
  const {authUser, user, loading} = authContext;

  const fadeAnim = useRef(new Animated.Value(1)).current

  const [render, setRender] = useState('log-in-home')
  const [accountDetails, setAccountDetails] = useState(null)

  const switchContent = (content) => {
    Animated.timing(
    fadeAnim,
    {
      toValue: 0,
      duration: 150,
      useNativeDriver: true
    }
    ).start();

    setTimeout(() => {
      setRender(content)
      Animated.timing(
      fadeAnim,
      {
        toValue: 1,
        duration: 150,
        useNativeDriver: true
      }
    ).start();
    }, 200)
  }

  useEffect(() => {
    authUser ? setRender('add-account-details') : setRender('log-in-home')
  }, [authUser])

  return (
    <Screen style={ROOT}>
      <ImageBackground 
        source={{uri: 'https://images.unsplash.com/photo-1598718544285-7180f670198b?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=987&q=80'}}
        imageStyle={{opacity:0.5}}
        style={{
          flex: 1,
        
          justifyContent: "center",
          alignItems: "center"
        }}
        
      >
        <View
          style={MODAL}
        >
          <Animated.View
            style={{

              flex: 1,
              opacity: fadeAnim
            }}
          >

            { render === 'log-in-home' ?
              <LogInHome
                setContent={(content) => switchContent(content)}
              />
              : render === 'create-account-home' ?
              <CreateAccountHome 
                setContent={(content) => switchContent(content)}
              />
              : render === 'sign-in-email' ?
              <SignInEmail 
                setContent={(content) => switchContent(content)}
              />
              : render === 'create-account-email' ?
              <CreateAccountEmail 
                setContent={(content) => switchContent(content)}
              />
              : render === 'add-account-details' ?
              <AddAccountDetails 
                setContent={(content) => switchContent(content)}
                setAccountDetails={(accountDetails) => setAccountDetails(accountDetails)}
              />
              : render === 'security-questions' ?
              <SecurityQuestions 
                setContent={(content) => switchContent(content)}
                accountDetails={accountDetails} 
              />
              : null
            }
          </Animated.View>
        </View>
      </ImageBackground>
    </Screen>
  )
}

const ROOT: ViewStyle = {
  backgroundColor: color.palette.black,
  flex: 1,
}

const MODAL: ViewStyle = {
  backgroundColor: `rgba(192, 192, 192, 0.6)`, // TODO: find a background blur solution

  width: `80%`, // TODO: mess w these on an iPad + landscape
  minHeight: `60%`,
  padding: 30,
  borderRadius: 10,
 
  justifyContent: `center`
}