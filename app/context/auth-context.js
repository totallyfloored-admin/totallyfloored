import React, { useContext, useState, useEffect } from 'react'
const AuthContext = React.createContext()
const AuthFunctionsContext = React.createContext()
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

export const useAuthContext = () => {
  return useContext(AuthContext)
}

export const useAuthFunctionsContext = () => {
  return useContext(AuthFunctionsContext)
}

export const AuthProvider = ({ children }) => {
  const [authUser, setAuthUser] = useState(null);
  const [user, setUser] = useState(null);
  const [loading, setLoading] = useState(true);

  const setFbUser = (d) => {
    console.log('firestore user data', d);
    console.log('user id', authUser.uid);
    d._exists && setUser(d);
    setLoading(false);
  }

  onError = (e) => {
    console.log('error, still onboarding', e);
    setLoading(false);

  }

  useEffect(() => {
    const authListener = auth().onAuthStateChanged((user) => {
      setAuthUser(user);
      !user && setLoading(false); // user is logged out!
    });
    return () => {
      authListener();
  }
  }, []);

  useEffect(() => {
    if (authUser) {
      const userListener = firestore().collection('users').doc(authUser.uid).onSnapshot(setFbUser, onError);

      return () => {
        userListener();
      }
    }
  }, [authUser])

  return (
    <AuthContext.Provider value={{ authUser: authUser, user: user, loading: loading }}>
      <AuthFunctionsContext.Provider
        value={{
          // PASS ANY FUNCTIONS THAT UI NEEDS HERE
        }}
      >
        {children}
      </AuthFunctionsContext.Provider>
    </AuthContext.Provider>
  )
}
