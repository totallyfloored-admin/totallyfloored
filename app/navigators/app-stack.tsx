import React from "react"
import { createNativeStackNavigator } from "@react-navigation/native-stack"
import { HomeScreen } from "../screens"
const Stack = createNativeStackNavigator()

export const AppStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName="home"
    >
      
      <Stack.Screen name="home" component={HomeScreen} />

    </Stack.Navigator>
  )
}
