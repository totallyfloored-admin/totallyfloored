import React from "react"
import { NavigationContainer } from "@react-navigation/native"
import { createNativeStackNavigator } from "@react-navigation/native-stack"
import { navigationRef } from "./navigation-utilities"
import { AuthStack, AppStack } from "./index"
import { useAuthContext } from '../context/auth-context'
import { LoadingScreen } from '../screens'

// Documentation: https://reactnavigation.org/docs/stack-navigator/
const Stack = createNativeStackNavigator()

export const AppNavigator = () => {
  const authContext = useAuthContext();
  const {authUser, user, loading} = authContext;

  return (
    <NavigationContainer
      ref={navigationRef}
    >
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}
        
      >
        { loading &&
          <Stack.Screen name="loading" component={LoadingScreen} />
        }
        { !authUser || !user ?
          <Stack.Screen name="authStack" component={AuthStack} />
          :
          <Stack.Screen name="homeStack" component={AppStack} />
        }
      </Stack.Navigator>
    </NavigationContainer>
  )
}

const exitRoutes = ["welcome"]
export const canExit = (routeName: string) => exitRoutes.includes(routeName)
