import React from "react"
import { StyleProp, TextInput, TextInputProps, ViewStyle } from "react-native"

export interface TextFieldProps extends TextInputProps {
  placeholder?: string
  style?: StyleProp<ViewStyle>
  forwardedRef?: any
}

export const TextField = (props: TextFieldProps) => {
  const {
    placeholder,
    style,
    style: styleOverride,
    forwardedRef,
    ...rest
  } = props

  return (
    <>
      <TextInput
        placeholder={placeholder}
        {...rest}
        style={[TEXT_FIELD, style]}
        ref={forwardedRef}
      />
    </>
  )
}

const TEXT_FIELD = {
  backgroundColor: `#efefef`,
  borderWidth: 1,
  borderColor: `#333`,
  borderRadius: 8,
  paddingHorizontal: 10,
  height: 48
}