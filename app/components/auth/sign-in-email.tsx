import React, { useState } from 'react';
import { Text, View, ViewStyle, TextStyle } from 'react-native';
import { Button, TextField } from '../index';
import { useAuthFunctionsContext } from '../../context/auth-context'
import auth from '@react-native-firebase/auth';

export const SignInEmail = (props) => {

    const authFunctions = useAuthFunctionsContext();

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [errorMessage, setErrorMessage] = useState('');

    const SignIn = async () => {
        auth()
            .signInWithEmailAndPassword(email, password)
            .then(() => {
                console.log('User account signed in!');
                return true;
            })
            .catch(error => {
                if (error.code === 'auth/email-already-in-use') {
                    setErrorMessage('Email already in use!')

                    console.log('That email address is already in use!');
                } else

                    if (error.code === 'auth/invalid-email') {
                        setErrorMessage('Email address is invalid!')
                        console.log('That email address is invalid!');
                    }
                    else 
                        if (error.code === 'auth/invalid-password') {
                            setErrorMessage('Email and Password combination is invalid!')
                            console.log('Email and Password combination is invalid!');
                        }
                        else {
                            setErrorMessage(error.code)
                        }
                        console.error(error);
                        return false;
                    });
    }

    return (
        <View style={{ flex: 1 }}>
            <Text style={TITLE}>
                Sign in with email
            </Text>

            <View style={{ flex: 1, marginTop: 20 }}>

                <TextField
                    placeholder='email address'
                    value={email}
                    onChangeText={(v) => setEmail(v)}
                    style={{
                        marginBottom: 15
                    }}
                />
                <TextField
                    placeholder='password'
                    value={password}
                    onChangeText={(v) => setPassword(v)}
                    style={{
                        marginBottom: 10
                    }}
                />
                <Text
                    style={[TEXT_BUTTON, { textAlign: `right` }]}

                >
                    Forgot Password?
                </Text>
            </View>

            <Button
                style={{ marginBottom: 15 }}
                text="Log In"

                onPress={SignIn}
            />

            <Text style={{ textAlign: `center` }}>
                {`Don't have an account? `}
                <Text
                    style={TEXT_BUTTON}
                    onPress={() => props.setContent('log-in-home')}
                >
                    Create one
                </Text>
            </Text>
        </View>
    )
}

const TITLE: TextStyle = {
    alignSelf: `center`,
    fontSize: 20,
    fontWeight: `600`,
    marginBottom: 25
}

const TEXT_BUTTON: TextStyle = {

    fontWeight: `600`,
    textDecorationLine: `underline`,

}