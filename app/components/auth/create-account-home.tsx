import React from 'react';
import { View, Text, TextStyle } from 'react-native';
import { Button } from '../index';
import { useAuthFunctionsContext } from '../../context/auth-context'
import { EmailIcon, AppleIcon, GoogleIcon } from '../../../assets/svg'

export const CreateAccountHome = ( props ) => {

    const authFunctions = useAuthFunctionsContext();

    return (
        <View style={{ flex: 1 }}>
            <Text style={TITLE}>
                create account with email
            </Text>
            <View style={{ flex: 1, justifyContent: `center` }}>

            <Button
                style={{ marginBottom: 20 }}
                text='Sign up with Apple'
                onPress={() => authFunctions.signUpWithApple()}
                icon={AppleIcon}
                    iconSize={38}
                    iconStyle={{
                        right: 9
                }}
            />

            <Button
                style={{ marginBottom: 20 }}
                text='Sign up with Google'
                onPress={() => authFunctions.signUpWithGoogle()}
                icon={GoogleIcon}
            />

            
            <Button
                style={{ marginBottom: 20 }}
                text='Sign up with Email'
                onPress={() => props.setContent('create-account-email')}
                icon={EmailIcon}
            />
            </View>
            <Text style={{ textAlign: `center` }}>
                {`Already have an account? `}
                <Text
                    style={TEXT_BUTTON}
                    onPress={() => props.setContent('log-in-home')}
                >
                    Log in
                </Text>
            </Text>
        </View>
    )
}

const TITLE: TextStyle = {
    alignSelf: `center`,
    fontSize: 20,
    fontWeight: `600`,
    marginBottom: 25
}

const TEXT_BUTTON: TextStyle = {
    fontWeight: `600`,
    textDecorationLine: `underline`,
}