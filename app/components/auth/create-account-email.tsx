import React, { useState } from 'react';
import { Text, View, ViewStyle, TextStyle } from 'react-native';
import { Button, TextField } from '../index';
import { useAuthFunctionsContext } from '../../context/auth-context'
import auth from '@react-native-firebase/auth';

export const CreateAccountEmail = (props) => {

    const authFunctions = useAuthFunctionsContext();

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [passwordTwo, setPasswordTwo] = useState('');
    const [errorMessage, setErrorMessage] = useState('');

    const createAccount = async () => {
        auth()
            .createUserWithEmailAndPassword(email, password)
            .then(() => {
                console.log('User account created & signed in!');
                return true;
            })
            .catch(error => {
                if (error.code === 'auth/email-already-in-use') {
                    setErrorMessage('Email already in use!')

                    console.log('That email address is already in use!');
                } else

                    if (error.code === 'auth/invalid-email') {
                        setErrorMessage('Email address is invalid!')
                        console.log('That email address is invalid!');
                    }
                    else {
                        setErrorMessage(error.code)
                    }
                console.error(error);
                return false;
            });


        console.log('sign UP with email', email, password);
    }

    return (
        <View style={{ flex: 1 }}>
            <Text style={TITLE}>
                Sign up with email
            </Text>

            <View style={{ flex: 1, marginTop: 20 }}>

                <TextField
                    placeholder='email address'
                    value={email}
                    onChangeText={(v) => setEmail(v)}
                    style={{
                        marginBottom: 15
                    }}
                />
                <TextField
                    placeholder='password'
                    value={password}
                    onChangeText={(v) => setPassword(v)}
                    style={{
                        marginBottom: 10
                    }}
                />
                <TextField
                    placeholder='password again'
                    value={passwordTwo}
                    onChangeText={(v) => setPasswordTwo(v)}
                    style={{
                        marginBottom: 10
                    }}
                />

                <Text>
                    {errorMessage}
                </Text>

            </View>

            <Button
                style={{ marginBottom: 15 }}
                text="Create Account"
                onPress={createAccount}
            />

            <Text style={{ textAlign: `center` }}>
                {`Already have an account? `}
                <Text
                    style={TEXT_BUTTON}
                    onPress={() => props.setContent('log-in-home')}
                >
                    Log in
                </Text>
            </Text>
        </View>
    )
}

const TITLE: TextStyle = {
    alignSelf: `center`,
    fontSize: 20,
    fontWeight: `600`,
    marginBottom: 25
}

const TEXT_BUTTON: TextStyle = {
    fontWeight: `600`,
    textDecorationLine: `underline`,
}