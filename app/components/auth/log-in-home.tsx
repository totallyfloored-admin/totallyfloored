import React from 'react';
import { Text, View, ViewStyle, TextStyle } from 'react-native';
import { Button } from '../index';
import { useAuthFunctionsContext } from '../../context/auth-context'
import { EmailIcon, AppleIcon, GoogleIcon } from '../../../assets/svg'
export const LogInHome = ( props ) => {

    const authFunctions = useAuthFunctionsContext();

    return (
        <View style={{ flex: 1 }}>
            <Text
                style={TITLE}
            >
                {`Totally\nFloored`}
            </Text>
            
            <View style={BUTTON_BAR}>
                <Button 
                    style={BUTTON}
                    text="Sign in with Email"
                    onPress={() => props.setContent('sign-in-email')}
                    icon={EmailIcon}
                />
                <Button 
                    style={[BUTTON, { backgroundColor: `#777`}]}
                    text="Sign in with Apple"
                    onPress={() => authFunctions.signInWithApple()}
                    icon={AppleIcon}
                    iconSize={38}
                    iconStyle={{
                        right: 9
                    }}
                />
                <Button 
                    style={BUTTON}
                    text="Sign in with Google"
                    onPress={() => authFunctions.signInWithGoogle()}
                    icon={GoogleIcon}
                />
                <View style={BREAKER} />
                <Button 
                    text="Create Account"
                    onPress={() => props.setContent('create-account-home')}
                />
            </View>
        </View>
        
    )
}

const TITLE: TextStyle = {
    fontSize: 20,
    fontWeight: `600`,
    letterSpacing: 1,
    textAlign: `center`,
    lineHeight: 30,
    textTransform: 'uppercase',
    marginTop: 10,
    marginBottom: 10,
}

const BUTTON_BAR: ViewStyle = {
    marginVertical: 20,
    flex: 1,
    justifyContent: `center`
}

const BUTTON: ViewStyle = {
    marginBottom: 15
}

const BREAKER: ViewStyle = {
    alignSelf: `center`,
    backgroundColor: `#777`,
    height: 1,
    width: `70%`,
    marginBottom: 15
}