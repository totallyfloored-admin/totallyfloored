import React, { useState } from 'react';
import { Text, View, ViewStyle, TextStyle } from 'react-native';
import { Button, TextField } from '../index';
import { useAuthFunctionsContext } from '../../context/auth-context'
import auth from '@react-native-firebase/auth';

export const AddAccountDetails = ( props ) => {

    const authFunctions = useAuthFunctionsContext();

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [jobTitle, setJobTitle] = useState('');
    const [companyName, setCompanyName] = useState('');
    const [error, setError] = useState('');

    const validateAndSubmit = () => {
        if (!firstName || !lastName || !jobTitle || !companyName)
        {
            setError('All fields must be filled out before continuing.')
        }
        else
        {
            props.setAccountDetails({firstName, lastName, jobTitle, companyName})
            props.setContent('security-questions')
        }
    }
    const logout = () => {
        auth()
        .signOut()
        .then(() => console.log('User signed out!'));
      }

    return (
        <View style={{ flex: 1 }}>
            <Text style={TITLE}>
                Add Account Details
            </Text>

            <View style={{ flex: 1, marginTop: 20 }}>

                <TextField 
                    placeholder='First Name'
                    value={firstName}
                    onChangeText={(v) => setFirstName(v)}
                    style={{
                        marginBottom: 15
                    }}
                />
                <TextField 
                    placeholder='Last Name'
                    value={lastName}
                    onChangeText={(v) => setLastName(v)}
                    style={{
                        marginBottom: 10
                    }}
                />
                <TextField 
                    placeholder='Job Title'
                    value={jobTitle}
                    onChangeText={(v) => setJobTitle(v)}
                    style={{
                        marginBottom: 10
                    }}
                />
                <TextField 
                    placeholder='Company Name'
                    value={companyName}
                    onChangeText={(v) => setCompanyName(v)}
                    style={{
                        marginBottom: 10
                    }}
                />
                
            </View>

            <Button
                style={{ marginBottom: 15 }}
                text="Submit"
                onPress={validateAndSubmit}
            />

            <Text style={{ textAlign: `center` }}>
                
                <Text
                    style={TEXT_BUTTON}
                    onPress={logout}
                >
                    {`Cancel`}
                </Text>
            </Text>
        </View>
    )
}

const TITLE: TextStyle = {
    alignSelf: `center`,
    fontSize: 20,
    fontWeight: `600`,
    marginBottom: 25
}

const TEXT_BUTTON: TextStyle = {
    fontWeight: `600`,
    textDecorationLine: `underline`,
}