import React, { useState } from 'react';
import { Text, View, ViewStyle, TextStyle } from 'react-native';
import { Button, TextField } from '../index';
import { useAuthFunctionsContext } from '../../context/auth-context'
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import { useAuthContext } from '../../context/auth-context.js'

export const SecurityQuestions = ( props ) => {

    const authContext = useAuthContext();
    const authFunctions = useAuthFunctionsContext();

    const [securityQuestion, setSecurityQuestion] = useState('');
    const [securityAnswer, setSecurityAnswer] = useState('');

    const setUserDetails = () => {

        const accountDetails = props.accountDetails;

        firestore()
        .collection('users')
        .doc(authContext.authUser.uid)
        .set({
          firstName: accountDetails.firstName,
          lastName: accountDetails.lastName,
          jobTitle: accountDetails.jobTitle,
          companyName: accountDetails.companyName,
          securityQuestion: securityQuestion,
          securityAnswer: securityAnswer
        })
        .then(() => {
          console.log('User added!');
        });
    }
    
    const logout = () => {
        auth()
        .signOut()
        .then(() => console.log('User signed out!'));
      }

    return (
        <View style={{ flex: 1 }}>
            <Text style={TITLE}>
                Security Question and Answer
            </Text>

            <View style={{ flex: 1, marginTop: 20 }}>

                <TextField 
                    placeholder='Security Question?'
                    value={securityQuestion}
                    onChangeText={(v) => setSecurityQuestion(v)}
                    style={{
                        marginBottom: 15
                    }}
                />
                <TextField 
                    placeholder='Security Answer'
                    value={securityAnswer}
                    onChangeText={(v) => setSecurityAnswer(v)}
                    style={{
                        marginBottom: 10
                    }}
                />
            </View>

            <Button
                style={{ marginBottom: 15 }}
                text="Finish & Continue to Viewport"
                // onPress should login to app and store new values into firestore
                onPress={setUserDetails}
            />

            <Text style={{ textAlign: `center` }}>
                
                <Text
                     style={TEXT_BUTTON}
                     onPress={logout}
                >
                    {`Cancel`}
                </Text>
            </Text>
        </View>
    )
}

const TITLE: TextStyle = {
    alignSelf: `center`,
    fontSize: 20,
    fontWeight: `600`,
    marginBottom: 25
}

const TEXT_BUTTON: TextStyle = {
    fontWeight: `600`,
    textDecorationLine: `underline`,
}