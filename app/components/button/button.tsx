import * as React from "react"
import { Pressable, ViewStyle, TextStyle, View } from "react-native"
import { Text } from "../text/text"

export const Button = ({ text, icon, style, children, iconSize, iconStyle, onPress, ...rest }: 
  { 
      style?: any,
      text?: string, 
      icon?: object,
      iconStyle?: ViewStyle,
      iconSize?: number,
      children?: any,
      onPress?: Function
  }) => {

  const content = children || <Text text={text} style={BUTTON_TEXT} />
  const Icon = icon

  return (
    <Pressable 
      style={({pressed}) => [BUTTON, {backgroundColor: pressed ? `#999` : `#333`}, style ]}
      onPress={onPress}
      {...rest}
    >
      {
        icon &&
        <View style={GHOST}>
          <Icon color="white" width={iconSize || 20} height={iconSize || 20} style={iconStyle}  />
        </View>
      }
      {content}

      {
        icon &&
        <View 
          style={GHOST}
        />
      }
    </Pressable>
  )
}

const BUTTON: ViewStyle = {
  height: 48,
  paddingHorizontal: 20,
  borderRadius: 8,
  flexDirection: `row`,
  alignItems: `center`,
  justifyContent: `space-between`
}

const BUTTON_TEXT: TextStyle = {
  flex: 1,
  textAlign: `center`,
  fontSize: 16,
}

const GHOST: ViewStyle = {
  width: 30,
  alignContent: `center`,
  justifyContent: `center`
}